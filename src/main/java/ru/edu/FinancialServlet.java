package ru.edu;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/finance")
public class FinancialServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/finance.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int sum;

        try {
            sum = Integer.parseInt(req.getParameter("sum"));
            double percentage = Double.parseDouble(req.getParameter("percentage")) / 100;
            int years = Integer.parseInt(req.getParameter("years"));
            if (sum < 0) {
                req.setAttribute("sum", null);
            } else {
                if (sum > 50000) {
                    for (int i = 0; i < years; i++) {
                        sum += sum * percentage;
                    }
                }
                req.setAttribute("sum", sum);
            }
        } catch (NumberFormatException e) {
            req.setAttribute("sum", null);
        }

        getServletContext().getRequestDispatcher("/result.jsp").forward(req, resp);
    }
}
