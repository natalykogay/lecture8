<%@page isELIgnored="false"%>
<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri ="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html><body>
<c:choose>
   <c:when test="${sum<50000}">
      <h1>Ошибка</h1>
      <p>Минимальная сумма на момент открытия вклада 50 000 рублей</p>
   </c:when>
   <c:when test="${sum==null}">
     <h1>Ошибка</h1>
     <p>Неверный формат данных. Скорректируйте значения</p>
   </c:when>
   <c:otherwise>
     <h1>Результат</h1>
     <p>Итоговая сумма ${sum} рублей </p>
   </c:otherwise>
</c:choose>
</body></html>